"use client"
import { use, useEffect, useState } from 'react';
import {Box, Button, Checkbox, FormControl, FormControlLabel, FormGroup, Grid, Input, InputLabel, Paper, Stack, Typography} from '@mui/material';

import axios from 'axios';

export type User = {
  id: number | undefined;
  username: string | undefined;
  password: string | undefined;
  password_plaintext: string | undefined;
  creditcard: string | undefined;
  creditcardcvc: string | undefined;
  creditcardexpirationdate: string | undefined; 
};

export default function Home() {

  const [messagesAll, setMessagesAll] = useState<string[]>([]);

  const [loggedInUser, setLoggedInUser] = useState<User | undefined>(undefined);

  const [xssEnabled, setXssEnabled] = useState<boolean>(false);
  const [sdeEnabled, setSdeEnabled] = useState<boolean>(false);

  const refreshMessages = () => {
    axios.get(
      '/api/messages')
    .then((response) => {
      var messagesArray: string[]  = response.data.map((entry:{id: number,message: string}) => {
        return entry.message;
      });
      setMessagesAll(messagesArray);
    });
  }

useEffect(() => {
  refreshMessages();
}
, []);

  const PrijavaBox = () => {

    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    return (
      <Paper 
        sx={{ bgcolor: "#e0e0e0", p: 2, width: "fit-content", mt: 5 }} 
        elevation={3}
      >
      <Typography variant='h6'>Prijava</Typography>
      <Box 
        display={"flex"} 
        flexDirection={"column"} 
        width={"fit-content"}
      > 
        <FormControl sx={{mt:2}}>
          <InputLabel htmlFor="username-input">Korisničko ime:</InputLabel>
          <Input id="username-input" sx={{bgcolor: "#f0f0f0"}}
            onChange={(event) => {
                setUsername(event.target.value);
              }
            }
            value={username}
          />
        </FormControl>
        <FormControl sx={{mt:3}}>
          <InputLabel htmlFor="password-input">Lozinka:</InputLabel>
          <Input id="password-input" type={sdeEnabled?'text':'password'} sx={{bgcolor: "#f0f0f0"}}
            onChange={(event) => {
                setPassword(event.target.value);
              }
            }
            value={password}
          />
        </FormControl>
        <Button variant="contained" 
          sx={{mt: 3}}
          onClick={()=>{
            axios.post('/api/users/login', {
              username: username,
              password: password,
              vunerable: sdeEnabled
            })
            .then((response) => {
              if (response.status != 200)
              {
                alert("Pogrešno korisničko ime ili lozinka");
                return;
              }
              if (response.data.length == 0)
              {
                alert("Pogrešno korisničko ime ili lozinka");
                return;
              }
              setLoggedInUser(response.data[0]);
              console.log(response.data[0]);
            });
          }}
        >
          Prijavi se
        </Button>
      </Box>
    </Paper>
    )
  };

  const ProfileBox = () => {
    return (
      <Paper sx={{ bgcolor: "#e0e0e0", p: 2, width: "fit-content", mt: 5 }} elevation={3}>
        <Typography variant='h6'> <b>Profil korisnika:</b> {loggedInUser?.username}</Typography>
        <Box>
          {
          sdeEnabled ?
          <Box mt={3}>
            <Typography variant='body1'><b>Lozinka:</b> {loggedInUser?.password_plaintext}</Typography>
            <Typography variant='body1'><b>Broj kartice:</b> {loggedInUser?.creditcard}</Typography>
            <Typography variant='body1'><b>Datum isteka kartice:</b> {loggedInUser?.creditcardexpirationdate}</Typography>
            <Typography variant='body1'><b>CVC:</b> {loggedInUser?.creditcardcvc}</Typography>
          </Box>
          :
          <Box>
            <Typography variant='body1'><b>Broj kartice:</b> {loggedInUser?.creditcard?.replace(/.(?=.{4})/g, '*')}</Typography>
            <Typography variant='body1'><b>Datum isteka kartice:</b> {loggedInUser?.creditcardexpirationdate}</Typography>
          </Box>
          }
        </Box>
        <Button variant="contained"
          sx={{mt: 3}}
          onClick={()=>{
            setLoggedInUser(undefined);
          }}
        >
          Odjavi se
        </Button>
      </Paper>
    )
  };
  
  const Message = (props: {message: string}) => {
    const { message } = props;

    const [executed, setExecuted] = useState<boolean>(false);

    if (!xssEnabled) {
      return (
        <Paper
          sx={{
            bgcolor: "#eeeeee",
            p: 1,
            maxWidth: "300px",
            minHeight: "200px",
            mt: 3,
          }}
          elevation={1}
        >
          {message}
        </Paper>
      );
    } else {

      if (!executed) {
        const scriptEl = document.createRange().createContextualFragment(message);
        document.head.append(scriptEl);
        setExecuted(true);
      }

      return (
        <Paper
          sx={{
            bgcolor: "#eeeeee",
            p: 1,
            maxWidth: "300px",
            minHeight: "200px",
            mt: 3,
          }}
          elevation={1}
          id="scriptdiv"
        >
          <div dangerouslySetInnerHTML={{ __html: message }} />
        </Paper>
      );
    }
  };

  const MessageBoardBox = (props: {messages: string[]}) => {
      
    const {messages} = props;

    return (
      <Box width={"100%"}>
        <Typography variant='h6'>Poslane poruke:</Typography>
        <Grid container >
          {messages.map((message,index) => {
            return (
              <Grid item xs={3} key={"message_"+index}>
                <Message message={message}/>

              </Grid>
            )
          })}
        </Grid>
      </Box>
    )
  };

  const SendMessageBox = () => {
    
    const [message, setMessage] = useState<string>('');



    return (
      <Paper 
        sx={{ bgcolor: "#e0e0e0", p: 2, width: "700px",height:"250px", mt: 5 }} 
        elevation={3}
      >
      <Typography variant='h6'>Slanje poruke</Typography>
      <Box 
        display={"flex"} 
        flexDirection={"column"} 
        width={"100%"}
      > 
        <FormControl sx={{mt:2}}>
          <InputLabel htmlFor="SendMessagebox-input">Poruka</InputLabel>
          <Input id="SendMessagebox-input" 
            multiline 
            type='text' 
            rows={5} 
            fullWidth 
            sx={{bgcolor: "#f0f0f0"}}
            onChange={(event) => {
              setMessage(event.target.value);
            }}
            value={message}
          />
        </FormControl>
  
        <Button variant="contained" 
          sx={{mt: 3, width: "fit-content"}}
          onClick={()=>{
            axios.post('/api/messages', {
              message: message
            }).then((response) => {
              refreshMessages();
            });
          }}
        >
          Pošalji
        </Button>
      </Box>
    </Paper>
    )

  };
  
  return (
    <Box>
      <Box sx={{
        backgroundColor: "primary.main",
        width: "100%",
        py: 5,
        textAlign: "center",
        color: "white"
      }}>
        <Typography variant="h3">WEB2 Drugi projekt - Security</Typography>
        <Typography variant="h5">Izradio: <b>Neven Marciuš, 0036532299</b></Typography>
      </Box>
      <Box mt={5}>
        <Typography variant='h6'>Cross-site scripting (XSS)</Typography>
        <Typography variant='body1'>{"Ovo je jednostavan message board. Korisnik može poslati poruku, ta poruka se sprema na server, pa se sve poruke ispišu. Kada je ranjivost uključena, moguće je izvršavanje XSS-a. Na primjer, možemo napisati <script> html element u string formatu i ta skripta će se izvršiti. Ovo je takozvani pohranjeni XSS, gdje napadać izvršava zlonamjeran kod na svim računalima koji se spoje na stranicu."}</Typography>
        <FormGroup>
          <FormControlLabel control={
          <Checkbox  
            checked={xssEnabled}
            onChange={(event) => {
              setXssEnabled(event.target.checked);
            }}
          />
          } label={xssEnabled?"Isključi ranjivost":"Uključi ranjivost"} />
        </FormGroup>
        <Box display={"flex"} flexDirection={"row"}>
         <SendMessageBox/>
          {/* 
            napravi  pohranjeni xss 
            napravi input za poruku i gumb za slanje
            poruka se pohranjuje u bazi
            sve poruke se izlistavaju na stranici
            poruka može biti javascript kod koji će se izvršiti ako ranjivost bude uključena

            ukljucena ranjivost: appendaj u innter html
            iskljucena ranjivost: stavi ko {text} unutar diva
          */}
          <Box ml={5} width={"100%"}>
            <MessageBoardBox messages={messagesAll}/>
          </Box>
        </Box>
      </Box>
      <Box mt={5}>
        <Typography variant='h6'>Nesigurna pohrana osjetljivih podataka (Sensitive Data Exposure)</Typography>
        <Typography variant='body1'>{"Nakon prijave, otvara se profil s korisničkim podatcima. Kada je ranjivost uključena, lozinka se prikazuje u plaintext obliku i broj kartice (i ostali podatci za karticu) se isto prikazuju. Kada je ranjivost isključena, ti podatci se ili ne prikazuju ili prikazuju tako da je dio sakriven. Također se plaintext lozinka šalje preko mreže samo kada je ranjivost uključena."}</Typography>
        <FormGroup>
          <FormControlLabel control={
          <Checkbox  
            checked={sdeEnabled}
            onChange={(event) => {
              setSdeEnabled(event.target.checked);
              setLoggedInUser(undefined);
            }}
          />
          } label={sdeEnabled?"Isključi ranjivost":"Uključi ranjivost"} />
        </FormGroup>
        <Stack>
          {loggedInUser != undefined ? 
          <ProfileBox/> : <PrijavaBox/>
          }
              {/* 
            napravi simle login form sa korisničkim imenom i lozinkom
            kada je korisnik prijavljen:
              napravi gumb za otvaranje profila korisnika

              kad je uklkjucena rannjivost: prikazi plaintekst lozinku, prikazi broj kartice od korisnika ( i datum i cvc)
              kar je iskljucena ranjivost: prikazi zvjezdice umjesto lozinke, prikazi samo zadnje 4 znamenke kartice
          */}
        </Stack>
      </Box>
    </Box>
  )
}
