// GET endpoint that fetches all from lab2.messages
import { NextResponse } from 'next/server';

import conn from '@/lib/db';

export async function GET(req: Request){

    try {

       
        const query = `SELECT * FROM lab2.messages;`
        
        const result = await conn.query(query);

        return NextResponse.json(result.rows);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}

// POST endpoint that inserts into lab2.messages
export async function POST(req: Request){

    try {

        const payload = await req.json();
        const { message} = payload;
        const query = `INSERT INTO lab2.messages(message) VALUES ('${message}');`
        
        const result = await conn.query(query);

        return NextResponse.json(result.rows);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}