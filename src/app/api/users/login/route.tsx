// make endpoint that cathes POST request with user credentials
// and returns 200 for success and 401 for failure. also returns user id if suuccess

import { NextResponse } from 'next/server';
import conn from '@/lib/db';

import { createHash } from 'crypto';

// POST endpoint that inserts into lab2.messages
export async function POST(req: Request){

    try {
        const payload = await req.json();
        const { username, password, vunerable } = payload;

        if (vunerable) {
            const query = `SELECT username,password_plaintext,creditcard,creditcardcvc,creditcardexpirationdate FROM lab2.users WHERE username='${username}' AND password='${createHash('sha256').update(password).digest('hex')}'`;
            const result = await conn.query(query);
            return NextResponse.json(result.rows);
        }

        const query = `SELECT username,creditcard,creditcardexpirationdate FROM lab2.users WHERE username='${username}' AND password='${createHash('sha256').update(password).digest('hex')}'`;
        const result = await conn.query(query);
        result.rows.forEach((row: any) => {
            row.creditcard = row.creditcard.replace(/\d(?=\d{4})/g, '*');

        });

        return NextResponse.json(result.rows);
    } catch (error) {
        console.log(error);
        return NextResponse.json({ message: "Error caught" + error }, { status: 500 });
    }
}