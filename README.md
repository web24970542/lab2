2. laboratorijska vježba iz kolegija Napredni razvoj programske podršske za web

Izradio: Neven Marciuš

Tehnologija: next.js

Implementirane ranjivosti:
1. XSS
2. Sensitive Data Exposure

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
